module.exports = {
	rmdir: [ "public", "private" ],

	angularjs: {
		publicArt: {
			templates: {
				src: [ "src/angular/art/**/*.pug" ],
				dest: "public/assets/angular",
				concat: true,
				args: {
					filename: "art-templates.js",
					module: "art",
					transformUrl: function(url) {
						return "/angular/art/" + url;
					}
				}
			},
			build: {
				src: [ "src/angular/art/app.js", "src/angular/art/**/*.js" ],
				dest: "public/assets/angular",
				map: true,
				iife: true,
				concat: {
					name: "art.bundle",
					iife: true
				}
			},
			watch: {
				src: [ "src/angular/art/**/*.js", "src/angular/art/**/*.pug" ]
			}
		},
		publicCore: {
			templates: {
				src: [ "src/angular/core/**/*.pug" ],
				dest: "public/assets/angular",
				concat: true,
				args: {
					filename: "core-templates.js",
					module: "app.core",
					transformUrl: function(url) {
						return "/angular/core/" + url;
					}
				}
			},
			build: {
				src: [ "src/angular/core/app.js", "src/angular/core/**/*.js", "!src/angular/core/**/*.spec.js" ],
				dest: "public/assets/angular",
				map: true,
				iife: true,
				concat: {
					name: "app.core.bundle",
					iife: true
				}
			},
			watch: {
				src: [ "src/angular/core/**/*.js", "!src/angular/core/**/*.spec.js", "src/angular/core/**/*.pug" ]
			}
		},
		publicLogin: {
			templates: {
				src: [ "src/angular/login/**/*.pug" ],
				dest: "public/assets/angular",
				concat: true,
				args: {
					filename: "login-templates.js",
					module: "app",
					transformUrl: function(url) {
						return "/angular/login/" + url;
					}
				}
			},
			build: {
				src: [ "src/angular/login/app.js", "src/angular/login/**/*.js" ],
				dest: "public/assets/angular",
				map: true,
				iife: true,
				concat: {
					name: "app.login.bundle",
					iife: true
				}
			},
			watch: {
				src: [ "src/angular/login/**/*.js", "src/angular/login/**/*.pug" ]
			}
		},

		privateCabinet: {
			build: {
				src: [ "src/angular/cabinet/app.js", "src/angular/cabinet/**/*.js" ],
				dest: "private/cabinet/assets/angular",
				map: true,
				iife: true,
				concat: {
					name: "app.bundle",
					iife: true
				}
			}
		},
		privateAdmin: {
			build: {
				src: [ "src/angular/admin/app.js", "src/angular/admin/**/*.js" ],
				dest: "private/admin/assets/angular",
				map: true,
				iife: true,
				concat: {
					name: "app.bundle",
					iife: true
				}
			}
		},
	},

	js: {
		public: {
			build: {
				src: "src/js/*.js",
				dest: "public/assets/js"
			}
		},
		publicLibs: {
			build: {
				src: "src/js/libs/*.js",
				dest: "public/assets/js/libs"
			}
		}
	},

	pug: {
		public: {
			build: {
				src: [ 'src/views/index.pug' ],
				dest: 'public'
			},
			watch: {
				src: [ 'src/views/**/*.pug', 'src/views/.hide/**/*.pug' ]
			}
		},
		private: {
			build: {
				src: [ 'src/views/**/*.pug', '!src/views/.hide/**/*.pug', '!src/views/index.pug' ],
				dest: 'private'
			},
			watch: {
				src: ['src/views/**/*.pug', '!src/views/index.pug']
			}
		},

		angularPrivateCabinet: {
			build: {
				src: 'src/angular/cabinet/**/*.pug',
				dest: 'private/cabinet/assets/angular'
			}
		},
		angularPrivateAdmin: {
			build: {
				src: 'src/angular/admin/**/*.pug',
				dest: 'private/admin/assets/angular'
			}
		}
	},

	stylus: {
		public: {
			build: {
				src: 'src/css/common.styl',
				dest: 'public/assets/css',
			},
			watch: {
				src: 'src/css/**/*.*'
			}
		},
		angularjs: {
			build: {
				src: 'src/angular/**/*.styl',
				dest: 'public/assets/css',
				concat: 'angular.bundle'
			}
		}
	},

	components: {
		js: {
			angular: {
				src: [
					"src/bower/angular/angular.min",
					"src/bower/angular-animate/angular-animate.min",
					"src/bower/angular-aria/angular-aria.min",
					"src/bower/angular-messages/angular-messages.min",
					"src/bower/angular-material/angular-material.min",
					"src/bower/angular-ui-router/release/angular-ui-router.min",
					"src/bower/angular-perfect-scrollbar/src/angular-perfect-scrollbar.min",
					"src/bower/angular-highlightjs/build/angular-highlightjs.min",
					"src/bower/angular-datatables/dist/angular-datatables.min",
				],
				dest: "public/assets/js/libs",
				concat: "angular.core"
			},
			main: {
				src: [
					"src/bower/jquery/dist/jquery.min",
					"src/bower/datatables.net/js/jquery.dataTables.min",
					"src/bower/moment/min/moment.min",
					"src/bower/d3/d3.min",
					"src/bower/nvd3/build/nv.d3.min",
					"src/bower/angular-nvd3/dist/angular-nvd3.min",
					"src/bower/mobile-detect/mobile-detect.min",
					"src/bower/perfect-scrollbar/js/perfect-scrollbar.min",
				],
				dest: "public/assets/js/libs",
			}
		},
		css: {
			main: {
				src: [
					"src/bower/angular-material/angular-material.min",
					"src/bower/angular/angular-csp",
					"src/bower/angular-datatables/dist/css/angular-datatables.min",
					"src/bower/perfect-scrollbar/css/perfect-scrollbar.min",
				],
				dest: "public/assets/css/libs"
			}
		},
		images: {
			main: {
				src: "src/images/**/*",
				dest: "public/assets/images"
			}
		},
		fonts: {
			"material-design-iconsets": {
				src: [ "src/bower/material-design-iconsets/iconsets/**/*" ],
				dest: "public/assets/fonts/icons/material-design"
			}
		},
		other: {
			favicon: {
				src: "src/other/favicon.ico",
				dest: "public"
			}
		}
	}
};