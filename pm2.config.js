module.exports = {
	apps : [{
		name            : "borrow.me",
		script          : "./index.js",
		watch           : ["server","public","private"],

		instances       : 0,
		exec_mode       : "cluster",

		env             : {
			"NODE_ENV": "development",
		},
		env_production  : {
			"NODE_ENV": "production"
		},

		merge_logs      : true,
		log_date_format : "YYYY-MM-DD HH:mm:ss Z"
	}]
};