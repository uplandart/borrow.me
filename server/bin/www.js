const http = require("http");

const App = require("../app");

const Server = http.createServer(App.callback());
const port = App.config.server.port;

Server.listen(port);
Server.on('error', onError);
Server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	let bind = typeof port === 'string'
			? 'Pipe ' + port
			: 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
	let addr = Server.address();
	let bind = typeof addr === 'string'
			? 'pipe ' + addr
			: 'port ' + addr.port;
	console.log(`Server(${App.env}) start listening on ${bind}`);
}