module.exports = {

	isAuth: async (ctx, next) => {
		if (ctx.isAuthenticated()) {
			await next();
		} else {
			ctx.status = 401;
		}
	},
	isAuthOrRedirect: (redirect_url) => {
		redirect_url = redirect_url || "/";
		return async (ctx, next) => {
			if (ctx.isAuthenticated()) {
				await next();
			} else {
				ctx.redirect(redirect_url);
			}
		}
	},

	isNotRole: async function () {
		let roles = Array.prototype.slice.call(arguments);
		return async (ctx, next) => {
			if (ctx.isAuthenticated() && !roles.includes(ctx.req.user.role)) {
				await next();
			} else {
				ctx.status = 401;
			}
		}
	},
	isRole: async function () {
		let roles = Array.prototype.slice.call(arguments);
		return async (ctx, next) => {
			if (ctx.isAuthenticated() && roles.includes(ctx.req.user.role)) {
				await next();
			} else {
				ctx.status = 401;
			}
		}
	},
	isNotAdmin: async (ctx, next) => {
		if (ctx.isAuthenticated() && ctx.req.user.role !== "admin") {
			await next();
		} else {
			ctx.status = 401;
		}
	},
	isAdmin: async (ctx, next) => {
		if (ctx.isAuthenticated() && ctx.req.user.role === "admin") {
			await next();
		} else {
			ctx.status = 401;
		}
	},
	isAdminOrRedirect: (redirect_url) => {
		redirect_url = redirect_url || "/";
		return async (ctx, next) => {
			if (ctx.isAuthenticated() && ctx.req.user.role === "admin") {
				await next();
			} else {
				ctx.redirect(redirect_url);
			}
		}
	},

	orRedirect: (redirect_url) => {
		redirect_url = redirect_url || "/";
		return async (ctx, next) => {
			if (ctx.isAuthenticated()) {
				await next();
			} else {
				ctx.redirect(redirect_url);
			}
		}
	},
	orError: async (ctx, next) => {
		// console.log("AUTH checkOrError", ctx.isAuthenticated(), ctx.req.user);
		if (ctx.isAuthenticated()) {
			await next();
		} else {
			ctx.status = 401;
		}
	}

};