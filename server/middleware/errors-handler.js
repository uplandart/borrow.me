const serializeError = require("serialize-error");

module.exports = (App) => {

	/**
	 * middleware will display errors for development mode
	 * and write to DB for production mode
	 */
	return async (err, ctx, next) => {
		App.dbh(function* (db) {

			if (!App.config.debug) {
				let req = ctx.request;
				yield db.checkInsert(`logs.errors RETURNING id`, {
					request: {
						httpVersion: req.httpVersion,
						url: req.url,
						method: req.method,
						upgrade: req.upgrade,
						headers: req.headers,
						cookies: req.cookies,
						params: req.params,
						query: req.query,
						body: req.body,
					},
					error: serializeError(err)
				});
			} else {
				console.trace(err);
			}

			if (err instanceof App.errors.BadAuthenticationError) {
				ctx.status(err.statusCode).body = err.message;
				return;
			}

			let message = err instanceof App.errors.ToUserError ? err.message : App.errors.DEFAULT_ERR_MESSAGE;
			App.config.debug && (message = err instanceof Error ? err.message : err);

			ctx.status(500).body = message;
		}).catch(err => {
			console.trace(err);
			ctx.status(500).body = App.errors.UNKNOWN_ERR_MESSAGE;
		});
	}
};