const DEFAULT_ERR_MESSAGE = "Произошёл сбой в работе сайта! Приносим свои извинения за доставленные неудобства!";
const UNKNOWN_ERR_MESSAGE = "Unknown error! Please report it as soon as possible with technical support!";

class ToUserError extends Error {
	constructor(message) {
		super(message);
		this.name = this.constructor.name;

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = (new Error(message)).stack;
		}
	}
}

class BadAuthenticationError extends Error {
	constructor(message=null) {
		super("Bad Authentication");
		this.name = this.constructor.name;
		this.statusCode = 400;
		this._message = message;

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = (new Error("Bad Authentication")).stack;
		}
	}
}

class UnauthorizedError extends BadAuthenticationError {
	constructor(message=null) {
		super(message);
		this.name = this.constructor.name;
		this.statusCode = 401;
		this.message = "Unauthorized";
		this._message = message;

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = (new Error("Unauthorized")).stack;
		}
	}
}

class ForbiddenError extends BadAuthenticationError {
	constructor(message=null) {
		super(message);
		this.name = this.constructor.name;
		this.statusCode = 403;
		this.message = "Forbidden";
		this._message = message;

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = (new Error("Forbidden")).stack;
		}
	}
}

module.exports = {
	DEFAULT_ERR_MESSAGE,
	UNKNOWN_ERR_MESSAGE,

	ToUserError,

	BadAuthenticationError,
	UnauthorizedError,
	ForbiddenError
};