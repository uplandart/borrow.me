const co = require("co");
const KoaPassport = require("koa-passport");

module.exports = (App) => {

	const passportCtrl = require("../controllers/passport/index")(App);
	const config = App.config.passport;

	if (config.local) {
		const LocalStrategy = require("passport-local").Strategy;

		if (config.local.signIn) {
			const configSignIn = config.local.signIn;

			/**
			 * Local Sign In
			 */
			KoaPassport.use(
					configSignIn.name,
					new LocalStrategy(
							{
								usernameField: configSignIn.usernameField,
								passwordField: configSignIn.passwordField,
								passReqToCallback: true // allows us to pass back the entire request to the callback
							},
							(req, username, password, done) => {
								co(async() => {
									let user = await passportCtrl.local.signIn.search(username, password);
									if (!user) throw new App.errors.ToUserError(configSignIn.flash.search);

									return done(null, user);
								}).catch(err => {
									console.trace(err);
									return done(err, false);
								});
							}
					)
			);
		}
		if (config.local.signUp) {
			const configSignUp = config.local.signUp;

			/**
			 * Local Sign Up
			 */
			KoaPassport.use(
					configSignUp.name,
					new LocalStrategy(
							{
								usernameField: configSignUp.usernameField,
								passwordField: configSignUp.passwordField,
								passReqToCallback: true // allows us to pass back the entire request to the callback
							},
							(req, username, password, done) => {
								co(async() => {
									let user = await passportCtrl.local.signUp.search(username, password);
									if (user) throw new App.errors.ToUserError(configSignUp.flash.search);

									user = await passportCtrl.local.signUp.create(req, username, password);
									if (!user) throw new App.errors.ToUserError(configSignUp.flash.create);

									return done(null, user);
								}).catch(err => {
									console.trace(err);
									return done(err, false);
								});
							}
					)
			);
		}
	}

	KoaPassport.serializeUser((user, done) => {
		// app.console.logp('serializeUser', user);
		done(null, user[config.serializeUserField]);
	});

	KoaPassport.deserializeUser((userId, done) => {
		co(async () => {
			done(null, await passportCtrl.deserializeUserQuery(userId));
		}).catch(err => {
			console.trace(err);
			return done(null, false);
		});
	});

	App.use(KoaPassport.initialize());
	App.use(KoaPassport.session());
};