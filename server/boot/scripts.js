import errors from "../scripts/errors";

export default (App) => {
	return {
		errors
	};
}