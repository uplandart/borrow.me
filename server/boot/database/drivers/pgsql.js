const Promise = require("bluebird");
const co = require("co");
const pg = require("pg");
const Moment = require("moment");

pg.types.setTypeParser(1082, function(stringValue) {
	return stringValue;
});

function EXPR(s) {
	this.expr = s;
	this.toString = function() {
		return this.s;
	}
}

let clients = 0;

module.exports = (config, App) => {

	let name = config.name || "main";
	if (App.dbh.hasOwnProperty(name)) throw new Error(`dbh with name ${name} already exist`);
	App.dbh[name] = (callback) => {
		return new Promise( (m_ret, m_rej) => {

			pg.connect(config, (err, client, done) => {
				if (err) return m_rej(err);
				if(!client.ext_prepared) {
					client.ext_prepared =  {};
					client.prepered_cnt = 0;
					//set custom init here!
				}
				if (config.debug) {
					// console.log('ac poolSize: %d, availableObjects: %d', pool.getPoolSize(), pool.availableObjectsCount());
					console.log('open client. now '+ ++clients);
				}
				let db = {
					pg: pg
					, noValues: {}
					, Rollback() { throw this.noValues }
					, Expr(s) { return new EXPR(s) } //expression shell
					, internalExec(query, values) {
						if(!values ||
								Array.isArray(values) && !values.length
						) throw "no values for prepares statement "+ query;
						if(!client.ext_prepared[query]) {
							client.ext_prepared[query] = ++client.prepered_cnt;
						}
						query = {
							name: 'q_' + client.ext_prepared[query]
							, text: query
							, values:
									values === this.noValues ? []
											: Array.isArray(values) ? values
											: [ values ]
						};
						if (config.debug) {
							console.log('%s\tEXECUTING QUERY:\n%s\nWITH%s', Moment().format(), query.text, JSON.stringify(query.values));
						}
						return client.query(query);
					}
					, formatError(x, q) {
						return x +'\n in '+q.text + ' \n with ' + JSON.stringify(q.values);
					}
					, setCookie(name, value) {
						//select security.current_user()
						this.internalExec(`select set_config('glb.${name}', $1, false)`,
								value)
					}
					, internalSelect(query, values) {
						if(!query.match(/^\s*SELECT|^\s*WITH\s/)) query = 'SELECT ' + query;
						return this.internalExec(query, values);
					}
					, SelectAll(query, values) {
						let q = this.internalSelect(query, values);
						q.on('row', (row, result) => result.addRow(row));
						return new Promise( (r,e) => {
							q.on('end', result => r(result.rows));
							q.on('error', x => e(this.formatError(x,q)))
						})
					}
					, SelectRow(query, values) {
						return this.SelectAll(query, values).then(rows => rows[0])
					}
					, SelectValue(query, values) {
						return this.SelectAll(query, values)
								.then(rows => {
									if(!rows[0]) return null;
									for (let i in rows[0]) return rows[0][i];
									return null;
								});
					}
					, Exec(query, values) {
						let q = this.internalExec(query, values);
						q.on('row', (row, result) => result.addRow(row));
						return new Promise( (r,e) => {
							q.on('end', result => r(result));
							q.on('error', x => e(this.formatError(x,q)))
						})
					}
					, __splitMap(kv, a) {
						let k = [];
						let v = [];
						a = a || [];
						for(let i in kv) {
							k.push(i);
							if(kv[i] instanceof EXPR)
								v.push( kv[i] + '');
							else {
								a.push(kv[i]);
								v.push( '$' + a.length )
							}
						}
						return {k,v,a};
					}
					, __makeCond(keys, a) {
						let wkv = this.__splitMap(keys, a);
						return wkv.k.map( (k,i) => k + ' = ' + wkv.v[i]).join(' AND ')
					}
					, Insert(table, values) {
						let rt = '';
						if(table.match(/^\s*(\S+)\s+(RETURNING\s+.*)/)) {
							rt = RegExp.$2;
							table = RegExp.$1;
						}
						let kva = this.__splitMap(values);
						let query = `
INSERT INTO ${table} ( ${kva.k} )
VALUES ( ${kva.v} ) ${rt}`;
						return this.Exec(query, kva.a)
								.then(r=>	{
									if(!r.rows[0]) return null;
									for (let i in r.rows[0]) return r.rows[0][i];
									return null;
								});
					}
					, checkInsert(table, values) {
						return db.Insert(table, values)
								.then(insert => {
									// console.log('checkInsert', insert);
									if (!insert || insert <= 0)
										throw Error('Insert error');
									return insert;
								});
					}
					, Update(table, keys, values) {
						let kva = this.__splitMap(values);

						let query = `
UPDATE ${table} SET ${kva.k.map( (k,i)=> k + ' = '+kva.v[i] )}
WHERE ${this.__makeCond(keys, kva.a)}`;
						return this.Exec(query, kva.a);
					}
					, checkUpdate(table, keys, values) {
						return db.Update(table, keys, values)
								.then(update => {
									// console.log('checkUpdate', update);
									if (update.rowCount <= 0)
										throw Error('Update error');
									return update.rowCount;
								});
					}
					, Delete(table, keys) {
						let a = [];
						let query = `DELETE FROM ${table} WHERE ${this.__makeCond(keys, a)}`;
						return this.Exec(query, a);
					}
					, checkDelete(table, keys) {
						return db.Delete(table, keys)
								.then(del => {
									// console.log('checkUpdate', update);
									if (del.rowCount <= 0)
										throw Error('Update error');
									return del.rowCount;
								});
					}
					, Save: function(table, key, values) {
						for(let k in key) {
							if(key[k] === undefined || key[k] === null) {
								if(values)
									key[k] = this.Insert(`${table} RETURNING ${k}`, values);
								return key;
							}
						}
						if(values)
							return this.Update(table, key, values)
									.then( () => key);
						else
							return this.Delete(table, key)
									.then( () => key);
					}
					, SelectRowByKey(query, keys) {
						let a = [];
						return this.SelectRow(
								`${query} WHERE ${this.__makeCond(keys, a)} LIMIT 1`
								, a
						);
					}
					, SelectValueByKey(query, keys) {
						let a = [];
						return this.SelectValue(
								`${query} WHERE ${this.__makeCond(keys, a)} LIMIT 1`
								, a
						);
					}
					, Transaction: function* (cb) {
						let r;
						client.query('BEGIN');
						try {
							r = yield* cb();
						} catch(e) {
							client.query('ROLLBACK');
							if(e !== this.noValues)
								throw e;
						}
						client.query('COMMIT');
						return r;
					}
				};

				require("./pgsql/search-rows")(App, db);

				co(callback, db)
						.then(r => {
							done();
							if (config.debug) {
								console.log('dn close client. now '+ --clients);
								// console.log('dn poolSize: %d, availableObjects: %d', pool.getPoolSize(), pool.availableObjectsCount());
							}
							m_ret(r);
						})
						.catch(r => {
							done();
							if (config.debug) {
								console.log('er close client. now '+ --clients);
								// console.log('er poolSize: %d, availableObjects: %d', pool.getPoolSize(), pool.availableObjectsCount());
							}
							m_rej(r);
						})
			}); //pg.connect
		} );
	};
};