const Mongoose = require("mongoose");

const MongooseConnection = Mongoose.connection;

module.exports = (config, App) => {

	const url = `mongodb://${config.user}:${config.password}@${config.host}:${config.port}/${config.database}`;
	const options = {
		server: {
			auto_reconnect: true
		},
		promiseLibrary: require("bluebird")
	};

	let isConnectedBefore = false;

	const connect = () => {
		console.log('DB connecting', url);
		Mongoose.connect(url, options);
	};

	connect();

	MongooseConnection.on('error', () => {
		console.log('DATABASE: Could not connect to MongoDB');
	});

	MongooseConnection.on('disconnected', () => {
		console.log('DATABASE: Lost MongoDB connection...');
		// if (!isConnectedBefore)
		// 	connect();
	});
	MongooseConnection.on('connected', () => {
		isConnectedBefore = true;
		console.log(`DATABASE: Connection established to MongoDB ${config.database}`);
	});

	MongooseConnection.on('reconnected', () => {
		console.log('DATABASE: Reconnected to MongoDB');
	});

	/**
	 * Close the Mongoose connection, when receiving SIGINT
	 */
	process.on('SIGINT', () => {
		MongooseConnection.close(() => {
			console.log('DATABASE: Force to close the MongoDB conection');
			process.exit(0);
		});
	});
};