module.exports = (App, db) => {

	class SearchRowsQueryBind {
		static EQUAL(opt) {
			return Object.assign({
				q: "## = ??"
			}, opt);
		}
		static UPPER_LIKE(opt) {
			return Object.assign({
				q: "UPPER(##) LIKE '%' || ?? || '%'"
				, f: OP.String.toUpperCase
			}, opt);
		}
		static NUMBER_LIKE(opt) {
			return Object.assign({
				q: "##::text LIKE '%' || ?? || '%'"
			}, opt);
		}
		static DATE(opt) {
			return Object.assign({
				q: "##::text LIKE '%' || ?? || '%'"
			}, opt);
		}
	}

	const _default = {
		fields: '*',
		where: '1=1',
		args: [],
		query: {
			values: '',
			fields: { id: SearchRowsQueryBind.NUMBER_LIKE() },
			rules: { all: ['.*'] }
		},
		order: {
			value: 'id',
			direction: 'ASC',
			field: 'id'
		},
		page: 1,
		limit: 10
	};

	class SearchRows {
		constructor(options = null) {
			this.debug = process.env.NODE_ENV !== 'production';

			// this.options = options;

			// this.from = options.from;
			// this.fields = options.fields || this.default.fields;
			// this.where = options.hasOwnProperty('where') ? options.where : this.default.where;
			// this.args = options.args || this.default.args;

			this.from = null;
			this.fields = _default.fields;
			this.where = _default.where;
			this.args = _default.args;

			this.query = {
				values: _default.query.values,
				fields: _default.query.fields,
				rules: _default.query.rules
			};
			this.order = {
				values: [ _default.order.value ],
				directions: [ _default.order.direction ],
				fields: [ _default.order.field ]
			};

			this.page = _default.page;
			this.limit = _default.limit;

			this.offset = 0;
			this.isLimit = true;

			if (options) {
				this.setOptions(options);
			}
		}
		setOptions(options) {
			if (options.from) this.from = options.from;
			if (options.fields) this.fields = options.fields;
			if (options.where) this.where = options.where;
			if (options.args) this.args = options.args;

			if (options.query) {
				if (options.query.values)
					this.query.values = options.query.values;
				if (options.query.field)
					this.query.field = options.query.field;
				if (options.query.fields)
					this.query.fields = options.query.fields;
			}
			if (options.page) this.page = options.page;
			if (options.limit) this.limit = options.limit;
			if (options.offset) this.offset = options.offset;
			if (options.order) {
				if (options.order.values)
					this.order.values = options.order.values;
				if (options.order.directions)
					this.order.directions = options.order.directions;
				if (options.order.fields)
					this.order.fields = options.order.fields;
			}
		}
		setOption(key, value) {
			if (!this.hasOwnProperty(key)) throw new Error(`SearchRows option ${key} not exist`);
			this[key] = value;
		}

		/**
		 *
		 * @private
		 */
		_buildWhereQuery() {
			let where = this.where;
			if (this.query.values) {
				if (this.query.field === 'all' && !this.query.fields.hasOwnProperty('all')) {

					let _query = '';
					let is_init = true;
					for (let key in this.query.fields) {
						if (!this.query.fields.hasOwnProperty(key)) continue;
						let _field = this.query.fields[key];
						// console.log('field', _field);
						if (typeof _field === 'object') {
							let _a = _field.alias ? _field.alias : (_field.a ? _field.a : key);
							_query += `${!is_init?' OR ':''}${_field.s.replace('##', _a).replace('??', `$${this.args.length+1}`)}`;
							if (_field.f) {
								this.args.push(_field.f(this.query.values));
							} else {
								this.args.push(this.query.values);
							}
						} else if (typeof _field === 'string') {
							_query += `${!is_init?' OR ':''}${SearchRowsQueryBind.EQUAL().s.replace('##', key).replace('??', `$${this.args.length+1}`)}`;
							this.args.push(this.query.values);
						}
						if (is_init) is_init = false;
					}

					if (_query) {
						where += `${where.length?' AND ':' '}(${_query})`;
					}
				} else {

					let _query = '';
					for (let key in this.query.fields) {
						if (!this.query.fields.hasOwnProperty(key)) continue;
						if (this.query.field === key) {
							let _field = this.query.fields[key];
							// console.log('field', _field);
							if (typeof _field === 'object') {
								let _a = _field.alias ? _field.alias : (_field.a ? _field.a : key);
								_query += ` AND ${_field.s.replace('##', _a)}`;
								if (_field.f) {
									this.args.push(_field.f(this.query.values));
								} else {
									this.args.push(this.query.values);
								}
							} else if (typeof _field === 'string') {
								_query += ` AND ${OP.SelectListQuery.SIMPLE().s.replace('##', key)}`;
								this.args.push(this.query.values);
							}
							break;
						}
					}

					if (_query) {
						if (where)
							where += _query;
						else
							where = _query;
					}
				}
				console.log('result where', where);
			}
			return where;
		}

		/**
		 *
		 * @private
		 */
		_buildOrderQuery() {
			if (this.order.fields && this.order.fields.length) {
				for (let i_v = 0; i_v < this.order.values.length; i_v++) {
					let isFind = false;
					// console.log('value', this.order.values[i_v]);

					for (let i_f = 0; i_f < this.order.fields.length; i_f++) {
						let field = this.order.fields[i_f];
						let f_k, f_v;
						console.log('order field', field, typeof field);
						if (typeof field === 'object') {
							f_k = Object.keys(field)[0];
							f_v = field[f_k];
							console.log('order field object', f_k, f_v);
						} else {
							f_k = field;
							f_v = field;
						}
						console.log('order field is find', f_k, f_v, this.order.values[i_v], f_k === this.order.values[i_v]);
						if (f_k === this.order.values[i_v]) {
							this.order.values[i_v] = f_v;
							isFind = true;
							break;
						}
					}

					if (!isFind) {
						let field = this.order.fields[0];
						if (typeof field === 'object') {
							this.order.values[i_v] = field[Object.keys(field)[0]];
						} else {
							this.order.values[i_v] = field;
						}
					}
				}
			}
			return this.order.values.map((val, index) => {
				return `${val} ${this.order.directions[index]}`;
			}).join(', ');
		}

		async exec() {
			if (!this.from) throw new Error('SearchRows field "from" required');

			let where = this._buildWhereQuery();
			let order = this._buildOrderQuery();

			this.isLimit = Number.isInteger(this.limit);
			if (this.isLimit) {
				this.limit = Number(this.limit);
				this.page = Number(this.page);
				this.offset = this.page * this.limit - this.limit;
			}

			let cmd = { query: null, args: [] };
			cmd.query = `
${this.fields}
FROM ${this.from}
${where?`WHERE ${where}`:''}
ORDER BY ${order}
${this.isLimit?`LIMIT ${this.limit}`:''}
${this.isLimit?`OFFSET ${this.offset}`:''}
`;
			cmd.args = this.args.length ? this.args : db.noValues;

			if (this.debug) console.log('QUERY CMD', cmd.query);
			if (this.debug) console.log('QUERY ARGS', cmd.args);

			let rows = await db.SelectAll(cmd.query, cmd.args);
			if (this.debug) console.log('QUERY RESULT', rows);

			let isLastPage = rows.length <= this.limit;
			if (!isLastPage) rows.pop();

			return { rows, isLastPage, page: this.page, limit: this.limit };
		}
		static async fastExec(options) {
			return await new SearchRows(options).exec();
		}
	}
	SearchRows.QueryBind = new SearchRowsQueryBind();

	class SearchRowsCtx extends SearchRows {
		constructor(ctx, options = null) {
			super(options);

			this.ctx = ctx;

			this.placeholder = {
				query: {
					values: 'q',
					field: 'qF'
				},
				page: 'page',
				limit: 'limit',
				offset: 'offset',
				filter: 'filter'
			};
		}
		async exec() {
			this.query.values = this.ctx.checkBody(this.placeholder.query.values).default(this.query.values).trim().value;
			// this.query.field = this.ctx.checkBody(this.placeholder.query.field).default(this.query.field).trim().value;
			this.page = this.ctx.checkBody(this.placeholder.page).default(this.page).isInt().value;
			this.limit = this.ctx.checkBody(this.placeholder.limit).default(this.limit).value;
			this.offset = this.ctx.checkBody(this.placeholder.offset).default(this.offset).isInt().value;

			return await super.exec();
		}
		static async fastExec(ctx, options) {
			return await new SearchRowsCtx(ctx, options).exec();
		}
	}

	db.SearchRows = SearchRows;
	db.SearchRowsCtx = SearchRowsCtx;
};
