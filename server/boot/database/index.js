const PgSqlConnect = require("./drivers/pgsql");
const MongoConnect = require("./drivers/mongo");

module.exports = (App) => {

	const config = App.config.database;

	App.dbh = {};

	for (let i = 0; i < config.connections.length; i++) {
		let connect = config.connections[i];

		connect.hasOwnProperty("debug") || (connect.debug = App.config.debug);

		if (/mysql/.test(connect.driver)) {

		} else if (/(mongo|mongoose|mongodb)/.test(connect.driver)) {
			MongoConnect(connect, App);
		} else {
			PgSqlConnect(connect, App);
		}
	}
};