import auth from "../middleware/auth";

export default (App) => {
	return {
		auth
	};
}