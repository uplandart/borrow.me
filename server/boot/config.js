module.exports = {

	debug: process.env.NODE_ENV !== 'production',

	app: require("../config/app"),
	server: require("../config/server"),
	database: require("../config/database"),
	passport: require("../config/passport")
};