const KoaStaticCache = require("koa-static-cache");
const KoaBodyParser = require("koa-bodyparser");
const KoaSession = require("koa-session");
const KoaValidate = require("koa-validate");
const KoaConvert = require("koa-convert");
const KoaFavicon = require("koa-favicon");
const KoaErrors = require("koa-errors");

const config = require("./config");

module.exports = (App) => {

	App.config = config;
	App.errors = require("./errors");

	App.keys = config.server.keys;

	App.use(KoaFavicon(config.app.static.path.favicon));
	App.use(KoaStaticCache(config.app.static.path.publicAssets, config.app.static.options));
	App.use(KoaBodyParser());
	App.use(KoaSession(config.server.session, App));

	KoaValidate(App);

	require("./database/index")(App);
	require("./passport")(App);

	if (config.debug) {
		App.use(async (ctx, next) => {
			console.log(`${ctx.req.method} ${ctx.req.url}`);
			await next();
		});

		/**
		 * for convenient display errors
		 * ONLY for non Production mode
		 */
		// App.use(KoaConvert(KoaErrors()));
	}

	require("../routes/index")(App);

	App.use(App.requireMW("errors-handler")(App));
};