const bcrypt = require("bcrypt");

module.exports = (App) => {

	const dbh = App.dbh.main;

	/**
	 * Select list of users data
	 * @param ctx
	 */
	async function getAll(ctx) {
		ctx.body = await dbh(async(db) => {
			return await db.SearchRowsCtx.fastExec(ctx, {
				fields: "id, fullname",
				from: "sec.users",
				where: "is_blocked IS FALSE"
			});
		});
	}

	/**
	 * Select one user data by id
	 * @param ctx
	 */
	async function getById(ctx) {
		let id = Number(ctx.params.id);
		ctx.body = await dbh(async(db) => {
			return await db.SelectRowByKey(`sec.v_users`, { id });
		});
	}

	/**
	 * Create new user data with check exist
	 * @param ctx
	 */
	async function insertNew(ctx) {

		const role = ctx.checkBody("role").default("user").value;
		const fullname = ctx.checkBody("fullname").trim().notEmpty().value;
		const email = ctx.checkBody("email").isEmail().value;
		let password = ctx.checkBody("password").trim().notEmpty().value;

		if (ctx.errors) {
			ctx.status = 500;
			ctx.body = ctx.errors;
			return;
		}

		password = await bcrypt.hash(password, await bcrypt.genSalt(12));

		ctx.body = await dbh(async(db) => {
			let row = await db.SelectRowByKey(`id FROM sec.local_users`, { email });
			if (row) {
				throw new Error(`User with this email already exist`);
			}
			let id = await db.checkInsert(`sec.users RETURNING id`, { role, fullname });
			return await db.checkInsert(`sec.local_accounts RETURNING email`, { user_id: id , email, password });
		});
	}

	/**
	 * Update user data by id
	 * @param ctx
	 */
	async function updateById(ctx) {
		let id = Number(ctx.params.id);
		ctx.body = await dbh(async(db) => {
			return await db.SelectRowByKey(``, { id });
		});
	}

	return {
		getAll,
		getById,
		insertNew,
		updateById
	}
};