const KoaPassport = require("koa-passport");

module.exports = (App) => {

	const config = App.config.passport;

	const configLocal = config.local;

	async function localSignIn(ctx, next) {
		await KoaPassport.authenticate(
				configLocal.signIn.name,
				function (err, user, info, status) {
					// console.log("err, user, info, status");
					// console.log(err, user, info, status);
					if (err) throw err;
					else if (info) throw new App.script.errors.ToUserError(info.message || info);
					else {
						if (user) {
							ctx.logIn(user);
							console.log('user', user);
							let path = "/";
							switch (user.role) {
								case "admin":
									path += "admin";
									break;
								case "user":
									path += "cabinet";
									break;
							}
							ctx.body = { path };
						} else {
							throw new App.errors.ToUserError(`undefined error`);
						}
					}
				}
		)(ctx, next);
	}

	async function localSignUp(ctx, next) {
		await KoaPassport.authenticate(
				configLocal.signUp.name,
				function (err, user, info, status) {
					console.log("err, user, info, status");
					console.log(err, user, info, status);
					if (err) throw err;
					else if (info) throw new App.errors.ToUserError(info.message || info);
					else {
						if (user) {
							ctx.logIn(user);
							ctx.body = { path: '/cabinet' };
						} else {
							throw new App.errors.ToUserError(`undefined error`);
						}
					}
				}
		)(ctx, next);
	}

	function signOut(ctx) {
		ctx.logout();
		ctx.redirect("/");
	}

	return {
		localSignIn,
		localSignUp,

		signOut
	}
};