const KoaSend = require("koa-send");

module.exports = (App) => {

	async function index(ctx) {
		await KoaSend(ctx, "index.html", { root: App.config.app.static.path.public });
	}

	return {
		index
	}
};