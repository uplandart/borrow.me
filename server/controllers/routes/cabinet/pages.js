const Path = require("path");
const KoaSend = require("koa-send");

module.exports = (App) => {

	const PATH_TO_PRIVATE_DIR = Path.join(App.config.app.static.path.private);
	const PATH_TO_PRIVATE_CABINET_DIR = Path.join(PATH_TO_PRIVATE_DIR, "cabinet");

	async function index(ctx, next) {
		// console.log("cabinet pages index");
		await KoaSend(ctx, "index.html", { root: PATH_TO_PRIVATE_CABINET_DIR });
	}

	async function assets(ctx, next) {
		try {
			await KoaSend(ctx, ctx.path, { root: PATH_TO_PRIVATE_DIR });
		} catch(err) {
			console.trace(err);
			ctx.redirect("/cabinet");
		}
	}

	return {
		index,
		assets
	}
};