module.exports = (App) => {

	return {
		signIn: require("./sign-in")(App),
		signUp: require("./sign-up")(App)
	}
};