const bcrypt = require("bcrypt");

module.exports = (App) => {
	const dbh = App.dbh.main;

	async function search(email, password) {
		return await dbh(async(db) => {
			return await db.SelectRowByKey(`* FROM sec.v_users`, { local_email: email });
		});
	}

	async function create(req, email, password) {
		let fullname = req.body.fullname;
		if (!fullname) throw new App.script.errors.ToUserError("Full name required");

		return await dbh(async(db) => {
			let userId = await db.checkInsert(`sec.users RETURNING id`, {
				role: "user",
				fullname,
				email
			});

			let passwordHash = await bcrypt.hash(password, await bcrypt.genSalt(12));
			await db.checkInsert(`sec.local_accounts RETURNING email`, {
				email,
				user_id: userId,
				password: passwordHash
			});

			return await db.SelectRowByKey(`* FROM sec.v_users`, { id: userId });
		});
	}

	return {
		search,
		create
	}
};