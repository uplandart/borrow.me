const bcrypt = require("bcrypt");

module.exports = (App) => {
	const dbh = App.dbh.main;

	async function search(email, password) {
		return await dbh(async(db) => {
			let row = await db.SelectRowByKey(`* FROM sec.v_users`, { local_email: email });
			if (!row) return;

			if (await bcrypt.compare(password, row.local_password)) {
				return row;
			}
		});
	}

	return {
		search
	}
};