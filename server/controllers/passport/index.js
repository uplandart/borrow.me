module.exports = (App) => {
	const dbh = App.dbh.main;

	async function deserializeUserQuery(userId) {
		return await dbh(async(db) => {
			return await db.SelectRowByKey(`* FROM sec.v_users`, { id: userId });
		});
	}

	return {
		deserializeUserQuery,

		local: require("./local/index")(App)
	}
};