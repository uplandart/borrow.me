const Path = require("path");

const PATH_TO_ROOT_DIR = Path.join(__dirname, "..", "..");

module.exports = {
	static: {
		path: {
			root        : PATH_TO_ROOT_DIR,
			public      : Path.join(PATH_TO_ROOT_DIR, "public"),
			favicon     : Path.join(PATH_TO_ROOT_DIR, "public", "favicon.ico"),
			publicAssets: Path.join(PATH_TO_ROOT_DIR, "public", "assets"),
			private     : Path.join(PATH_TO_ROOT_DIR, "private")
		},
		options: {
			maxAge: 365 * 24 * 60 * 60
		}
	}
};