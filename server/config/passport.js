module.exports = {
	serializeUserField: "id",

	local: {
		signIn: {
			name: "local-sign-in",
			usernameField: "email",
			passwordField: "password",
			flash: {
				search: 'Wrong email or password'
			},
		},
		signUp: {
			name: "local-sign-up",
			usernameField: "email",
			passwordField: "password",
			flash: {
				search: 'User with this credentials already exist',
				create: 'Error create new user'
			}
		}
	}
};