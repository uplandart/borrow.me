module.exports = {

	connections: [
		{
			driver  : "pgsql",
			name    : "main",
			database: process.env.DB_NAME || "postgres",
			host    : process.env.DB_HOST || "127.0.0.1",
			port    : process.env.DB_PORT || 5432,
			user    : process.env.DB_USER || "postgres",
			password: process.env.DB_PASS || "postgres",
		},
		// {
		// 	driver  : "mongo",
		// 	name    : "mongo",
		// 	host    : process.env.DB_HOST || "127.0.0.1",
		// 	port    : process.env.DB_PORT || 27017,
		// 	database: process.env.DB_NAME || "test",
		// 	user    : process.env.DB_USER || "admin",
		// 	password: process.env.DB_PASS || "123"
		// }
	]
};