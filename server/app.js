const Koa = require("koa");
const Path = require("path");

const App = new Koa();

/**
 * Loads a module from server root
 * @param {String} path
 * @returns {*}
 */
App.require = function (path) {
	// console.log("PATH", path, typeof path);
	let _path = path.split("/");
	// return require(`${__dirname}${Path.sep}${path}`);
	return require(`${__dirname}${Path.sep}${_path.join(Path.sep)}`);
};

/**
 * Loads a controller module from server root
 * @param {String} path
 * @returns {*}
 */
App.requireCtrl = function (path) {
	return App.require(`controllers${Path.sep}${path}`);
};
/**
 * Loads a middleware module from server root
 * @param {String} path
 * @returns {*}
 */
App.requireMW = function (path) {
	return App.require(`middleware${Path.sep}${path}`);
};
/**
 * Loads a script module from server root
 * @param {String} path
 * @returns {*}
 */
App.requireScript = function (path) {
	return App.require(`scripts${Path.sep}${path}`);
};

require("./boot/index")(App);

module.exports = App;