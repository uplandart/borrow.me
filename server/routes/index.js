module.exports = (App) => {

	/**
	 * Login app
	 */
	App.use(require("./login/pages")(App).routes());
	App.use(require("./login/auth")(App).routes());

	/**
	 * Main app
	 */
	App.use(require("./cabinet/pages")(App).routes());
	App.use(require("./cabinet/api/index")(App).routes());

	/**
	 * Admin app
	 */
	App.use(require("./admin/pages")(App).routes());
	const adminApiRoutes = require("./admin/api/index")(App);
	App.use(adminApiRoutes.routes(), adminApiRoutes.allowedMethods());
};