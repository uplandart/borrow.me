const Router = new require("koa-router")({
	prefix: "/api"
});

module.exports = (App) => {

	const ctrlApi = App.requireCtrl("routes/cabinet/api/index")(App);
	const mwCheck = App.requireMW("check");

	/**
	 * Access only for authorization users
	 */

	Router.use(mwCheck.isAuth);

	Router.get("/", ctrlApi.index);

	return Router;
};