const Router = new require("koa-router")();

module.exports = (App) => {

	const pagesCtrl = App.requireCtrl("routes/cabinet/pages")(App);
	const mwCheck = App.requireMW("check");

	/**
	 * Access only for authorization users
	 */

	Router.get(/^\/cabinet/, mwCheck.isAuthOrRedirect());

	Router.get(/^\/cabinet(?:|\/)$/, pagesCtrl.index);
	Router.get(/^\/cabinet\/assets\/(.*)/, pagesCtrl.assets);

	return Router;
};