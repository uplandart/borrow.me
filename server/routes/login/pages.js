const Router = new require("koa-router")();

module.exports = (App) => {

	const pagesCtrl = App.requireCtrl("routes/login/pages")(App);

	Router.get(/^(\/|\/index(\.html)?)$/, pagesCtrl.index);

	return Router;
};