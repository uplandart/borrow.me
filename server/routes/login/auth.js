const KoaRouter = require("koa-router");

const Router = new KoaRouter({
	prefix: "/auth"
});

module.exports = (App) => {

	const config = App.config.passport;
	const authCtrl = App.requireCtrl("routes/login/auth")(App);
	// const authCtrl = AuthCtrl(App);

	if (config.local) {
		if (config.local.signIn) Router.post("/sign-in", authCtrl.localSignIn);
		if (config.local.signUp) Router.post("/sign-up", authCtrl.localSignUp);
	}

	Router.get("/sign-out", authCtrl.signOut);

	return Router;
};