const Router = new require("koa-router")({
	prefix: "/admin/api"
});

module.exports = (App) => {

	const ctrlApi = App.requireCtrl("routes/admin/api/index")(App);
	const mwCheck = App.requireMW("check");

	/**
	 * Access only for user with role equal "admin"
	 */

	Router.use(mwCheck.isAdmin);

	Router.get("/", ctrlApi.index);
	// Router.use(ApiUsersRouter(App).routes());

	return Router;
};