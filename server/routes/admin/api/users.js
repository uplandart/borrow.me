const Router = new require("koa-router")({
	prefix: "/users"
});

module.exports = (App) => {

	const ctrlApiUsers = App.requireCtrl("routes/admin/api/users")(App);

	Router.get("/", ctrlApiUsers.getAll);
	Router.get("/:id", ctrlApiUsers.getById);
	Router.post("/", ctrlApiUsers.insertNew);
	Router.put("/:id", ctrlApiUsers.updateById);

	return Router;
};