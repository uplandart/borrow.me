const Router = new require("koa-router")();

module.exports = (App) => {

	const ctrlPages = App.requireCtrl("routes/admin/pages")(App);
	const mwCheck = App.requireMW("check");

	/**
	 * Access only for user with role equal "admin"
	 */

	Router.get(/^\/admin/, mwCheck.isAdminOrRedirect());

	Router.get(/^\/admin(?:|\/)$/, ctrlPages.index);
	Router.get(/^\/admin\/assets\/(.*)/, ctrlPages.assets);

	return Router;
};