angular.module("app.core")
.filter('moment', () => {
	return (input, format = "DD.MM.YYYY HH:mm") => {
		try {
			return (input && moment(input).format(format)) || "";
		} catch(e) {
			return "Invalid date";
		}
	}
});