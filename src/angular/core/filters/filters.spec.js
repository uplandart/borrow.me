describe("Filters", () => {

	beforeEach(angular.mock.module('app.core'));

	describe("moment", () => {
		let momentFilter;

		beforeEach(inject(($filter) => {
			momentFilter = $filter("moment", {});
		}));

		it('should return empty string on undefined or null or "" income data', () => {
			expect(momentFilter(undefined)).toEqual("");
			expect(momentFilter(null)).toEqual("");
			expect(momentFilter("")).toEqual("");

			// expect(momentFilter(123)).not.toEqual("");
			// expect(momentFilter("123")).not.toEqual("");
		});

		it('should return "Invalid date" string', () => {
			let result = "Invalid date";

			console.log(momentFilter("05-25-2015"));
			// expect(momentFilter("25.05.2015")).toEqual(result);
			expect(momentFilter("25-05-2015")).toEqual(result);
		});

		it("should transform different format to date string", () => {
			let result = "25.05.2015 00:00";

			expect(momentFilter("2015-05-25")).toEqual(result);
			expect(momentFilter(new Date(2015,4,25))).toEqual(result);
		});

		it("should transform string to date with custom format", () => {
			expect(momentFilter("2015-05-25", "DD.MM.YYYY")).toEqual("25.05.2015");
		});
	});
});