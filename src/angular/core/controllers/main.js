angular.module('app.core')
.controller('MainCtrl', MainCtrl);

MainCtrl.$inject = ["$scope", "$rootScope"];
function MainCtrl($scope, $rootScope) {

	$scope.$on("$viewContentAnimationEnded", function(a) {
		a.targetScope.$id === $scope.$id && $rootScope.$broadcast("artSplashScreen::remove");
	})
}