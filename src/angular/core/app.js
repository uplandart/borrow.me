angular.module("app.core", [
	"ui.router",
	"ngAnimate", "ngMessages", "ngMaterial",
	"art", "hljs", "datatables"
]);

angular.module("app.core")
		.run(runCore);

runCore.$inject = ["$window", "$rootScope", "$location", "$timeout", "$state"];
function runCore($window, $rootScope, $location, $timeout, $state) {
	let deregisterStateChangeStartListener = $rootScope.$on("$stateChangeStart", () => {
				$rootScope.loadingProgress = true;
			}),
			deregisterStateChangeSuccessListener = $rootScope.$on("$stateChangeSuccess", () => {
				$timeout(() => {
					$window.ga("send", "pageview", {
						page: $location.url()
					});
					$rootScope.loadingProgress = false;
				})
			});

	$rootScope.state = $state;

	$rootScope.$on("$destroy", () => {
		deregisterStateChangeStartListener();
		deregisterStateChangeSuccessListener();
	});
}