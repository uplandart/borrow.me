angular.module('app.core')
.factory('cls', ClsFactory);

ClsFactory.$inject = ['http'];
function ClsFactory(http) {

	return {
		getItems(name) {
			return http.get(`/api/cls/${name}`, {});
		},
		getItem(name, id) {
			return http.get(`/api/cls/${name}/${id}`);
		},
		addItem(name, data) {
			return http.post(`/api/cls/${name}`, data);
		},
		updateItem(name, id, data) {
			return http.put(`/api/cls/${name}/${id}`, data);
		}
	}
}