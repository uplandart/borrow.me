angular.module('app.core')
.factory('UserProfile', UserProfile);

UserProfile.$inject = ['Auth', 'FloatDialogs'];
function UserProfile(Auth, FloatDialogs) {

	let userProfile = {};

	let clearUserProfile = () => {
		for (let prop in userProfile) {
			if (userProfile.hasOwnProperty(prop)) {
				delete userProfile[prop];
			}
		}
	};

	let fetchUserProfile = () => {
		return Auth.getProfile().then((response) => {
			clearUserProfile();
			return angular.extend(userProfile, response.data, {

				$refresh: fetchUserProfile,

				$isRole: function () {
					return Array.prototype.slice.call(arguments).indexOf(userProfile.role) >= 0;
				},
				$isNotRole: function () {
					return Array.prototype.slice.call(arguments).indexOf(userProfile.role) < 0;
				},

				$isAnonymous: () => {
					return userProfile.anonymous;
				},

				$isAuthenticated: () => {
					return !userProfile.anonymous;
				}

			});
		}).catch((err) => {
			console.log('Auth getProfile() error', err);
			if (err.status !== 401) {
				FloatDialogs.alert({
					title: 'Error!',
					content: err.data
				});
			}
		});
	};

	return fetchUserProfile();
}