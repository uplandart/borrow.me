angular.module("app.core").provider("appConfig", appConfig);

function appConfig() {
	function config(e) {
		t = angular.extend({}, t, e)
	}

	var t = {
		disableCustomScrollbars: !1,
		disableMdInkRippleOnMobile: !0,
		disableCustomScrollbarsOnMobile: !0
	};

	this.config = config;
	this.$get = function() {
		function getConfig(e) {
			return angular.isUndefined(t[e]) ? !1 : t[e]
		}

		function setConfig(e, a) {
			t[e] = a
		}
		var n = {
			getConfig: getConfig,
			setConfig: setConfig
		};
		return n
	}
}