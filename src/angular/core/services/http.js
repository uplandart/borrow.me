angular.module('app.core')
.factory('http', HttpFactory);

HttpFactory.$inject = ['$http'];
function HttpFactory($http) {

	function getHeaders(type) {
		let res = { 'Content-Type': 'application/json' };
		switch(type) {
			case 'form':
				res['Content-Type'] = 'application/x-www-form-urlencoded';
				break;
		}
		return res;
	}

	function withMethodHeadersType(method, url, data, headersType) {
		data = data || {};
		return $http({
			method: method,
			url: url,
			data: $.param(data),
			headers: getHeaders(headersType)
		});
	}

	function postForm(url, data) {
		return withMethodHeadersType('POST', url, data, 'form');
	}
	function putForm(url, data) {
		return withMethodHeadersType('PUT', url, data, 'form');
	}
	function post(url, data) {
		return withMethodHeadersType('POST', url, data, 'json');
	}
	function put(url, data) {
		return withMethodHeadersType('PUT', url, data, 'json');
	}
	function get(url, params) {
		params = params || {};
		return $http({
			method: 'GET',
			url: url,
			params: params
		});
	}

	return {
		get,
		post,
		put,
		postForm,
		putForm
	}
}