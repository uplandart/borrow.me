angular.module('app.core')
.service('Auth', Auth);

Auth.$inject = ['$http'];
function Auth($http) {

	this.getProfile = () => {
		return $http.get("/auth/sign-in");
	};

	this.signIn = (credentials) => {
		// console.log('Auth signIn() credentials', credentials);
		return $http.post("/auth/sign-in", credentials);
	};

	this.signUp = (credentials) => {
		// console.log('Auth signIn() credentials', credentials);
		return $http.post("/auth/sign-up", credentials);
	};
}