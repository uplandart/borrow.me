angular.module('app.core')
.factory('FloatDialogs', FloatDialogs);

FloatDialogs.$inject = ['$mdDialog', '$document'];
function FloatDialogs($mdDialog, $document) {

	let dialogs = {};

	dialogs.alert = (args) => {
		if (!args.title) throw new Error('Required argument "title"');
		if (!args.content) throw new Error('Required argument "content"');

		if (!args.hasOwnProperty('ariaLabel')) args.ariaLabel = 'notice-dialog alert label';
		if (!args.hasOwnProperty('btnOk')) args.btnOk = 'Ok';
		if (!args.hasOwnProperty('clickOutsideToClose')) args.clickOutsideToClose = false;

		return $mdDialog.show(
				$mdDialog.alert()
						.clickOutsideToClose(args.clickOutsideToClose)
						.title(args.title)
						.textContent(args.content)
						.ariaLabel(args.ariaLabel)
						.ok(args.btnOk)
						.targetEvent(args.event)
		);
	};

	dialogs.confirm = (args) => {
		if (!args.title) throw new Error('Required argument "title"');
		if (!args.content) throw new Error('Required argument "content"');

		if (!args.hasOwnProperty('ariaLabel')) args.ariaLabel = 'notice-dialog confirm label';
		if (!args.hasOwnProperty('btnOk')) args.btnOk = 'Оk';
		if (!args.hasOwnProperty('btnCancel')) args.btnCancel = 'Cancel';
		if (!args.hasOwnProperty('clickOutsideToClose')) args.clickOutsideToClose = false;

		return $mdDialog.show(
				$mdDialog.confirm()
						.clickOutsideToClose(args.clickOutsideToClose)
						.title(args.title)
						.textContent(args.content)
						.ariaLabel(args.ariaLabel)
						.targetEvent(args.event)
						.ok(args.btnOk)
						.cancel(args.btnCancel)
		);
	};

	dialogs.prompt = (args) => {
		if (!args.title) throw new Error('Required argument "title"');
		if (!args.content) throw new Error('Required argument "content"');

		if (!args.hasOwnProperty('placeholder')) args.placeholder = '';
		if (!args.hasOwnProperty('initVal')) args.initVal = '';
		if (!args.hasOwnProperty('ariaLabel')) args.ariaLabel = 'notice-dialog prompt label';
		if (!args.hasOwnProperty('btnOk')) args.btnOk = 'Оk';
		if (!args.hasOwnProperty('btnCancel')) args.btnCancel = 'Cancel';
		if (!args.hasOwnProperty('clickOutsideToClose')) args.clickOutsideToClose = false;

		return $mdDialog.show(
				$mdDialog.prompt()
						.clickOutsideToClose(args.clickOutsideToClose)
						.title(args.title)
						.textContent(args.content)
						.placeholder(args.placeholder)
						.initialValue(args.initVal)
						.ariaLabel(args.ariaLabel)
						.targetEvent(args.event)
						.ok(args.bntOk)
						.cancel(args.btnCancel)
		);
	};

	dialogs.advanced = (args) => {
		if (!args.controller) throw new Error('Required argument "controller"');
		if (!args.templateUrl) throw new Error('Required argument "templateUrl"');

		if (!args.hasOwnProperty('clickOutsideToClose')) args.clickOutsideToClose = false;
		if (!args.hasOwnProperty('parent')) args.parent = angular.element($document.body);
		if (!args.hasOwnProperty('fullscreen')) args.fullscreen = true; // Only for -xs, -sm breakpoints.

		args.targetEvent = args.event;

		return $mdDialog.show(args);
	};

	return dialogs;
}