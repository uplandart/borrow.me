describe("http factory", () => {
	let http, $httpBackend;

	beforeEach(angular.mock.module('app.core'));

	beforeEach(inject((_$httpBackend_, _http_) => {
		$httpBackend = _$httpBackend_;
		http = _http_;
	}));

	afterEach(() => {
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
	});

	it("should exist", () => {
		expect(http).toBeDefined();
	});

	describe(".get", () => {
		it("should exist", () => {
			expect(angular.isFunction(http.get)).toBe(true);
		});

		it("should return one cls object", () => {
			let url = "/api/users?a=1&b=2";
			let expectedData = { test: "bla" };
			$httpBackend
					.expectGET(url)
					.respond(200, expectedData);

			let actualResponse;
			http.get(url)
					.then((response) => {
						actualResponse = response;
					});

			$httpBackend.flush();

			expect(actualResponse.config.url).toEqual(url);
			expect(actualResponse.status).toEqual(200);
			expect(actualResponse.data).toEqual(expectedData);
		});

		it("should return 400 error", () => {
			let url = "/api/user";
			let expectedData = { test: "bla" };
			$httpBackend
					.expectGET(url)
					.respond(500, expectedData);

			let actualResponse;
			http.get(url)
					.catch(err => {
						actualResponse = err;
					});

			$httpBackend.flush();

			expect(actualResponse.config.url).toEqual(url);
			expect(actualResponse.status).toEqual(500);
			expect(actualResponse.data).toEqual(expectedData);
		});
	});
});