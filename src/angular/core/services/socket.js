angular.module('app.core')
.factory('socket', SocketFactory);

SocketFactory.$inject = ['socketFactory'];
function SocketFactory(socketFactory) {
	return socketFactory({
		prefix: 'chat:',
		ioSocket: io()
	});
}