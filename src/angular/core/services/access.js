angular.module('app.core')
.factory('Access', Access);

Access.$inject = ['$q', 'UserProfile'];
function Access($q, UserProfile) {

	let Access = {
		OK: 200,

		// "we don't know who you are, so we can't say if you're authorized to access
		// this resource or not yet, please sign in first"
		UNAUTHORIZED: 401,

		// "we know who you are, and your profile does not allow you to access this resource"
		FORBIDDEN: 403,

		isRole: function () {
			return UserProfile.then((userProfile) => {
				if (userProfile.$isRole.call(arguments)) {
					return Access.OK;
				} else if (userProfile.$isAnonymous()) {
					return $q.reject(Access.UNAUTHORIZED);
				} else {
					return $q.reject(Access.FORBIDDEN);
				}
			});
		},

		isNotRole: function () {
			return UserProfile.then((userProfile) => {
				if (userProfile.$isNotRole.call(arguments)) {
					return Access.OK;
				} else if (userProfile.$isAnonymous()) {
					return $q.reject(Access.UNAUTHORIZED);
				} else {
					return $q.reject(Access.FORBIDDEN);
				}
			});
		},

		isAnonymous: () => {
			return UserProfile.then((userProfile) => {
				if (userProfile.$isAnonymous()) {
					return Access.OK;
				} else {
					return $q.reject(Access.FORBIDDEN);
				}
			});
		},

		isAuthenticated: () => {
			return UserProfile.then((userProfile) => {
				console.log("Access isAuthenticated() userProfile", userProfile);
				if (userProfile.$isAuthenticated()) {
					return Access.OK;
				} else {
					console.log("Access isAuthenticated() userProfile wrong", Access.UNAUTHORIZED);
					return $q.reject(Access.UNAUTHORIZED);
				}
			});
		}
	};

	return Access;
}