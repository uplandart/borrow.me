angular.module('app')
.config(ConfigRoutes);

ConfigRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
function ConfigRoutes($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/auth/login');
	$stateProvider

			.state("app", {
				"abstract": true,
				views: {
					"main@": {
						templateUrl: "/angular/core/directives/layout/content-only/content-only.html",
						controller: "MainCtrl as vm"
					}
				}
			})

			.state("app.auth_login", {
				url:"/auth/login",
				views: {
					"main@":{
						templateUrl: "/angular/core/directives/layout/content-only/content-only.html",
						controller: "MainCtrl as vm"
					},
					"content@app.auth_login": {
						templateUrl: "/angular/login/pages/auth/login/directives/login.html",
						controller: "LoginCtrl as vm"
					}
				}
			})
			.state("app.auth_register", {
				url:"/auth/register",
				views: {
					"main@":{
						templateUrl: "/angular/core/directives/layout/content-only/content-only.html",
						controller: "MainCtrl as vm"
					},
					"content@app.auth_register": {
						templateUrl: "/angular/login/pages/auth/register/directives/register.html",
						controller: "RegisterCtrl as vm"
					}
				}
			})
			.state("app.auth_forgot_password", {
				url:"/auth/forgot-password",
				views: {
					"main@":{
						templateUrl: "/angular/core/directives/layout/content-only/content-only.html",
						controller: "MainCtrl as vm"
					},
					"content@app.auth_forgot_password": {
						templateUrl: "/angular/login/pages/auth/forgot-password/directives/forgot-password.html",
						controller: "ForgotPasswordCtrl as vm"
					}
				}
			})

}