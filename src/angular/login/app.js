angular.module("app", [
	"app.core"
])
.config(Config);

Config.$inject = ['$qProvider', '$mdIconProvider'];
function Config($qProvider, $mdIconProvider) {

	$qProvider.errorOnUnhandledRejections(false);

	$mdIconProvider
			.iconSet('action', 'fonts/icons/material-design/action-icons.svg', 24)
			.iconSet('alert', 'fonts/icons/material-design/alert-icons.svg', 24)
			.iconSet('av', 'fonts/icons/material-design/av-icons.svg', 24)
			.iconSet('communication', 'fonts/icons/material-design/communication-icons.svg', 24)
			.iconSet('content', 'fonts/icons/material-design/content-icons.svg', 24)
			.iconSet('device', 'fonts/icons/material-design/device-icons.svg', 24)
			.iconSet('editor', 'fonts/icons/material-design/editor-icons.svg', 24)
			.iconSet('file', 'fonts/icons/material-design/file-icons.svg', 24)
			.iconSet('hardware', 'fonts/icons/material-design/hardware-icons.svg', 24)
			.iconSet('icons', 'fonts/icons/material-design/icons-icons.svg', 24)
			.iconSet('image', 'fonts/icons/material-design/image-icons.svg', 24)
			.iconSet('maps', 'fonts/icons/material-design/maps-icons.svg', 24)
			.iconSet('navigation', 'fonts/icons/material-design/navigation-icons.svg', 24)
			.iconSet('notification', 'fonts/icons/material-design/notification-icons.svg', 24)
			.iconSet('social', 'fonts/icons/material-design/social-icons.svg', 24)
			.iconSet('toggle', 'fonts/icons/material-design/toggle-icons.svg', 24)
			.defaultIconSet('fonts/icons/material-design/mdi-icons.svg', 24);
}