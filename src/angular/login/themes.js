angular.module('app')
.config(ConfigThemes);

ConfigThemes.$inject = ['$mdThemingProvider'];
function ConfigThemes($mdThemingProvider) {

	$mdThemingProvider.theme("default")
			.primaryPalette("blue-grey")
			.accentPalette("blue")
			.warnPalette("red");

	$mdThemingProvider.setDefaultTheme('default');
	// This is the absolutely vital part, without this, changes will not cascade down through the DOM.
	$mdThemingProvider.alwaysWatchTheme(true);
}