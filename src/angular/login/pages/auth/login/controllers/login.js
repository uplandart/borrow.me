angular.module("app")
.controller("LoginCtrl", LoginCtrl);

LoginCtrl.$inject = ["$window", "FloatDialogs", "Auth"];
function LoginCtrl($window, FloatDialogs, Auth) {
	let vm = this;

	vm.credentials = {};

	vm.login = () => {
		Auth.signIn(vm.credentials).then((res) => {
			// Redirect user somewhere
			$window.location.href = res.data.path;
		}).catch((err) => {
			console.log("LoginCtrl login() error", err);
			FloatDialogs.alert({
				title: "Error!",
				content: err.data
			});
		});
	};
}