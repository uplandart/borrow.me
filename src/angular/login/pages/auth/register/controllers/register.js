angular.module("app")
.controller("RegisterCtrl", RegisterCtrl);

RegisterCtrl.$inject = ["$window", "Auth", "FloatDialogs"];
function RegisterCtrl($window, Auth, FloatDialogs) {
	let vm = this;

	vm.credentials = {};

	vm.register = () => {
		Auth.signUp(vm.credentials).then((res) => {
			// Redirect user somewhere
			$window.location.href = res.data.path;
		}).catch((err) => {
			console.log("LoginCtrl login() error", err);
			FloatDialogs.alert({
				title: "Error!",
				content: err.data
			});
		});
	};
}