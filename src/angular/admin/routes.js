angular.module('app')
.config(ConfigRoutes);

ConfigRoutes.$inject = ['$stateProvider', '$urlRouterProvider', 'artNavigationServiceProvider'];
function ConfigRoutes($stateProvider, $urlRouterProvider, artNavigationServiceProvider) {

	$urlRouterProvider.otherwise('/home');
	$stateProvider

			.state("app", {
				"abstract": true,
				views: {
					"main@": {
						templateUrl: "/angular/core/directives/layout/vertical-navigation/vertical-navigation.html",
						controller: "MainCtrl as vm"
					},
					"toolbar@app": {
						templateUrl: "/admin/assets/angular/common/components/toolbar/toolbar.html",
						controller: "ToolbarCtrl as vm"
					},
					"navigation@app": {
						templateUrl: "/admin/assets/angular/common/components/navigation/navigation.html",
						controller: "NavigationCtrl as vm"
					}
				}
			})

			.state("app.home", {
				url:"/home",
				views: {
					"content@app": {
						templateUrl: "/admin/assets/angular/pages/home/directives/home.html",
						controller: "HomeCtrl as vm"
					}
				},
				bodyClass: "home"
			})

			.state("app.users", {
				url:"/users",
				views: {
					"content@app": {
						templateUrl: "/admin/assets/angular/pages/users/directives/users.html",
						controller: "UsersCtrl as vm"
					}
				},
				bodyClass: "users"
			});

	artNavigationServiceProvider.saveItem("apps", {
		title: "MENU",
		group: true,
		weight: 1
	});
	artNavigationServiceProvider.saveItem("apps.home", {
		title: "Home",
		icon: "home",
		state: "app.home",
		weight: 9
	});
	artNavigationServiceProvider.saveItem("apps.users", {
		title: "Users",
		icon: "account-multiple",
		state: "app.users",
		badge: {
			content: 3,
			color: "#FF6F00",
			title: "Count of new users"
		},
		weight: 9
	});
}