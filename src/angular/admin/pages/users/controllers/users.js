angular.module("app")
.controller("UsersCtrl", UsersCtrl);

UsersCtrl.$inject = ["$mdSidenav"];
function UsersCtrl($mdSidenav) {
	let vm = this;

	vm.toggleSidenav = (e) => {
		$mdSidenav(e).toggle();
	};

	vm.openNewUserDialog = ($event) => {

	}
}