angular.module('app.core')
		.controller('ToolbarCtrl', ToolbarCtrl);

ToolbarCtrl.$inject = ["$window", "$rootScope", "$q", "$state", "$timeout", "$mdSidenav", "$mdToast", "artNavigationService"];
function ToolbarCtrl($window, e, t, a, n, i, s, l) {
	let vm = this;

	function logout() {
		$window.location.href = "/auth/sign-out";
	}

	function r(e) {
		i(e).toggle()
	}

	function u() {
		vm.bodyEl.toggleClass("art-navigation-horizontal-mobile-menu-active")
	}

	function toggleArtNavigationFolded() {
		console.log('ToolbarCtrl toggleArtNavigationFolded');
		l.toggleFolded()
	}

	function g(e) {
		for (var a = [], i = l.getFlatNavigation(), o = t.defer(), s = 0; s < i.length; s++) i[s].uisref && a.push(i[s]);
		return e && (a = a.filter(function(t) {
			return angular.lowercase(t.title).search(angular.lowercase(e)) > -1 ? !0 : void 0
		})), n(function() {
			o.resolve(a)
		}, 1e3), o.promise
	}

	function v(e) {
		e.uisref && (e.stateParams ? a.go(e.state, e.stateParams) : a.go(e.state))
	}

	e.global = {
		search: ""
	};
	vm.bodyEl = angular.element("body");

	vm.toggleSidenav = r;
	vm.logout = logout;

	vm.toggleHorizontalMobileMenu = u;
	vm.toggleArtNavigationFolded = toggleArtNavigationFolded;
	vm.search = g;
	vm.searchResultClick = v;
}