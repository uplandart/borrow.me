angular.module('app.core')
		.controller('NavigationCtrl', NavigationCtrl);

NavigationCtrl.$inject = ["$scope"];
function NavigationCtrl(e) {
	let vm = this;

	function toggleArtNavigationFolded() {
		console.log('NavigationCtrl toggleArtNavigationFolded', vm.folded);
		vm.folded = !vm.folded;
	}

	vm.bodyEl = angular.element("body");
	vm.folded = !1;
	vm.artScrollOptions = {
		suppressScrollX: !0
	};
	vm.toggleArtNavigationFolded = toggleArtNavigationFolded;
	e.$on("$stateChangeSuccess", function() {
		vm.bodyEl.removeClass("art-navigation-horizontal-mobile-menu-active")
	})
}