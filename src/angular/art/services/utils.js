angular.module("art").factory("artUtils", artUtils);

artUtils.$inject = ["$window"];
function artUtils($window) {

	function exists(e, t) {
		return t.indexOf(e) > -1
	}

	function detectBrowser() {

		function getFromList(listItems) {
			for (let i = 0; i < listItems.length; i++) {
				let item = listItems[i];
				let itemString = item.string,
						itemProp = item.prop;
				versionSearch = item.versionSearch;

				if (versionSearch || item.identity && itemString) {
					if (-1 !== itemString.indexOf(item.subString)) return item.identity;
				} else if (itemProp) return item.identity;
			}
		}

		function getVersion(e) {
			let t = e.indexOf(versionSearch);
			if (-1 !== t) return parseInt(e.substring(t + versionSearch.length + 1));
		}

		if (resultDetectBrowser) return resultDetectBrowser;

		var listKnownBrowsers = [{
					string: $window.navigator.userAgent,
					subString: "Edge",
					versionSearch: "Edge",
					identity: "Edge"
				}, {
					string: $window.navigator.userAgent,
					subString: "Chrome",
					identity: "Chrome"
				}, {
					string: $window.navigator.userAgent,
					subString: "OmniWeb",
					versionSearch: "OmniWeb/",
					identity: "OmniWeb"
				}, {
					string: $window.navigator.vendor,
					subString: "Apple",
					versionSearch: "Version",
					identity: "Safari"
				}, {
					prop: $window.opera,
					identity: "Opera"
				}, {
					string: $window.navigator.vendor,
					subString: "iCab",
					identity: "iCab"
				}, {
					string: $window.navigator.vendor,
					subString: "KDE",
					identity: "Konqueror"
				}, {
					string: $window.navigator.userAgent,
					subString: "Firefox",
					identity: "Firefox"
				}, {
					string: $window.navigator.vendor,
					subString: "Camino",
					identity: "Camino"
				}, {
					string: $window.navigator.userAgent,
					subString: "Netscape",
					identity: "Netscape"
				}, {
					string: $window.navigator.userAgent,
					subString: "MSIE",
					identity: "Explorer",
					versionSearch: "MSIE"
				}, {
					string: $window.navigator.userAgent,
					subString: "Trident/7",
					identity: "Explorer",
					versionSearch: "rv"
				}, {
					string: $window.navigator.userAgent,
					subString: "Gecko",
					identity: "Mozilla",
					versionSearch: "rv"
				}, {
					string: $window.navigator.userAgent,
					subString: "Mozilla",
					identity: "Netscape",
					versionSearch: "Mozilla"
				}];
			var listKnownOS = [{
					string: $window.navigator.platform,
					subString: "Win",
					identity: "Windows"
				}, {
					string: $window.navigator.platform,
					subString: "Mac",
					identity: "Mac"
				}, {
					string: $window.navigator.platform,
					subString: "Linux",
					identity: "Linux"
				}, {
					string: $window.navigator.platform,
					subString: "iPhone",
					identity: "iPhone"
				}, {
					string: $window.navigator.platform,
					subString: "iPod",
					identity: "iPod"
				}, {
					string: $window.navigator.platform,
					subString: "iPad",
					identity: "iPad"
				}, {
					string: $window.navigator.platform,
					subString: "Android",
					identity: "Android"
				}];

			var versionSearch = "",
				browser = getFromList(listKnownBrowsers) || "unknown-browser",
				version = getVersion($window.navigator.userAgent) || getVersion($window.navigator.appVersion) || "unknown-version",
				os = getFromList(listKnownOS) || "unknown-os";

		browser = browser.toLowerCase();
		version = browser + "-" + version;
		os = os.toLowerCase();

		return resultDetectBrowser = {
			browser,
			version,
			os
		};
	}

	function guidGenerator() {
		var gen = function() {
			return (65536 * (1 + Math.random()) || 0).toString(16).substring(1);
		};
		return gen() + gen() + gen() + gen() + gen() + gen();
	}

	function isMobile() {
		return mobileDetect.mobile();
	}

	function toggleInArray(e, t) {
		if (t.indexOf(e) === -1) {
			t.push(e);
		} else {
			t.splice(t.indexOf(e), 1);
		}
	}

	var mobileDetect = new MobileDetect($window.navigator.userAgent),
			resultDetectBrowser = null;

	return {
		exists,
		detectBrowser,
		guidGenerator,
		isMobile,
		toggleInArray
	};
}