angular.module("art.scroll").provider("artScrollConfig", artScrollConfig);

function artScrollConfig() {
	function e(e) {
		t = angular.extend({}, t, e)
	}
	var t = {
		wheelSpeed: 1,
		wheelPropagation: !1,
		swipePropagation: !0,
		minScrollbarLength: null,
		maxScrollbarLength: null,
		useBothWheelAxes: !1,
		useKeyboard: !0,
		suppressScrollX: !1,
		suppressScrollY: !1,
		scrollXMarginOffset: 0,
		scrollYMarginOffset: 0,
		stopPropagationOnClick: !0
	};
	this.config = e, this.$get = function() {
		function e() {
			return t
		}
		var a = {
			getConfig: e
		};
		return a
	}
}