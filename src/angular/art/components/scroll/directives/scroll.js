angular.module("art.scroll").directive("artScroll", artScroll);

artScroll.$inject = ["$timeout", "artScrollConfig", "artUtils", "appConfig"];
function artScroll($timeout, artScrollConfig, artUtils, appConfig) {
	return {
		restrict: "AE",
		compile: function(tElement) {
			return appConfig.getConfig("disableCustomScrollbars") || appConfig.getConfig("disableCustomScrollbarsOnMobile") && artUtils.isMobile()
					? void 0
					: (tElement.addClass("art-scroll"), function(scope, element, attributes) {

				function onMouseEnterListener() {
					PerfectScrollbar.update(element[0]);
				}
				
				let s = {};
				attributes.artScroll && (s = scope.$eval(attributes.artScroll));
				s = angular.extend({}, artScrollConfig.getConfig(), s);
				
				$timeout(function() {
					PerfectScrollbar.initialize(element[0], s);
				}, 0);
				
				element.on("mouseenter", onMouseEnterListener);
				
				scope.$watch(function() {
					return element.prop("scrollHeight");
				}, function(e, t) {
					angular.isUndefined(e) || angular.equals(e, t) || onMouseEnterListener();
				});
				scope.$watch(function() {
					return element.prop("scrollWidth")
				}, function(e, t) {
					angular.isUndefined(e) || angular.equals(e, t) || onMouseEnterListener();
				});
				
				scope.$on("$destroy", function() {
					element.off("mouseenter");
					PerfectScrollbar.destroy(element[0]);
				});
			})
		}
	}
}