angular.module("art.navigation").directive("artNavigation", artNavigation);

artNavigation.$inject = ["$rootScope", "$timeout", "$mdSidenav", "artNavigationService"];
function artNavigation($rootScope, $timeout, $mdSidenav, artNavigationService) {
	return {
		restrict: "E",
		scope: {
			folded: "=",
			root: "@"
		},
		controller: "ArtNavigationController as vm",
		templateUrl: "/angular/art/components/navigation/directives/navigation.html",
		transclude: true,
		compile: function(tElement) {
			tElement.addClass("art-navigation");

			return function(scope, element) {

				let elBody = angular.element("body"),
						elNavigationFoldExpander = angular.element('<div id="art-navigation-fold-expander"></div>'),
						elNavigationFoldCollapser = angular.element('<div id="art-navigation-fold-collapser"></div>'),
						sidenavNavigation = $mdSidenav("navigation");

				artNavigationService.setNavigationScope(scope);
				init();

				scope.$watch(() => {
					return sidenavNavigation.isLockedOpen()
				}, function(newVal, oldVal) {
					if (!angular.isUndefined(newVal) && !angular.equals(newVal, oldVal)) {
						let f = artNavigationService.getFolded();
						if (f)
							if (newVal) $rootScope.$broadcast("artNavigation::collapse");
							else {
								let o = artNavigationService.getActiveItem();
								o && o.scope.$emit("artNavigation::stateMatched");
							}
					}
				});
				scope.$watch("folded", function(newVal, oldVal) {
					if (!angular.isUndefined(newVal) && !angular.equals(newVal, oldVal))
						l(newVal);
				});

				scope.toggleFolded = function() {
					let f = artNavigationService.getFolded();
					l(!f);
				};

				scope.$on("$stateChangeStart", function() {
					sidenavNavigation.close();
				});
				scope.$on("$destroy", function() {
					elNavigationFoldCollapser.off("mouseenter touchstart");
					elNavigationFoldExpander.off("mouseenter touchstart");
				});

				function init() {
					if (!artNavigationService.getFolded()) {
						artNavigationService.setFolded(scope.folded);
					} else {
						$timeout(() => {
							$rootScope.$broadcast("artNavigation::collapse");
						});
						elBody.addClass("art-navigation-folded");
						appendElNavigationFoldExpander();
					}
				}

				function l(f) {
					artNavigationService.setFolded(f);
					if (f) {
						$rootScope.$broadcast("artNavigation::collapse");
						elBody.addClass("art-navigation-folded");
						appendElNavigationFoldExpander();
					} else {
						let a = artNavigationService.getActiveItem();
						a && a.scope.$emit("artNavigation::stateMatched");
						elBody.removeClass("art-navigation-folded art-navigation-folded-open");
						c();
					}
				}

				function appendElNavigationFoldExpander() {
					element.parent().append(elNavigationFoldExpander);
					$timeout(() => {
						elNavigationFoldExpander.on("mouseenter touchstart", m);
					});
				}

				function r() {
					elBody.find("#main").append(elNavigationFoldCollapser);
					elNavigationFoldCollapser.on("mouseenter touchstart", p);
				}

				function c() {
					elNavigationFoldCollapser.remove();
				}

				function m(e) {
					if (e) {
						e.preventDefault();
						artNavigationService.setFoldedOpen(true);
					}
					let a = artNavigationService.getActiveItem();
					if (a) {
						a.scope.$emit("artNavigation::stateMatched");
						elBody.addClass("art-navigation-folded-open");
						elNavigationFoldExpander.remove();
						r();
					}
				}

				function p(t) {
					if (t) {
						t.preventDefault();
						artNavigationService.setFoldedOpen(false);
						$rootScope.$broadcast("artNavigation::collapse");
						elBody.removeClass("art-navigation-folded-open");
						elNavigationFoldCollapser.remove();
						appendElNavigationFoldExpander();
					}
				}
			}
		}
	}
}