angular.module("art.navigation").directive("artNavigationItem", artNavigationItem);

function artNavigationItem() {
	return {
		restrict: "A",
		require: "^artNavigationNode",
		compile: function(tElement) {
			tElement.addClass("art-navigation-item");
			return function(scope, element, attributes, ctrl) {
				ctrl.collapsable && element.on("click", ctrl.toggleCollapsed);
				scope.$on("$destroy", function() {
					element.off("click")
				});
			}
		}
	}
}