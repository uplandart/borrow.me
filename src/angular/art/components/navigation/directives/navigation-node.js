angular.module("art.navigation").directive("artNavigationNode", artNavigationNode);

function artNavigationNode() {
	return {
		restrict: "A",
		bindToController: {
			node: "=artNavigationNode"
		},
		controller: "ArtNavigationNodeController as vm",
		compile: function(tElement) {
			tElement.addClass("art-navigation-node");
			return function(scope, element, attrs, ctrl) {
				element.addClass(ctrl.getClass());
				ctrl.group && element.addClass("group");
			}
		}
	}
}