angular.module("art.navigation").controller("ArtNavigationNodeController", ArtNavigationNodeController);

ArtNavigationNodeController.$inject = ["$scope", "$element", "$rootScope", "$animate", "$state", "artNavigationService"];
function ArtNavigationNodeController(e, t, a, n, i, o) {
	function s() {
		if (p.hasChildren = p.node.children.length > 0, p.group = !(!angular.isDefined(p.node.group) || p.node.group !== !0), !p.hasChildren || p.group ? p.collapsable = !1 : p.collapsable = !(!angular.isUndefined(p.node.collapsable) && "boolean" == typeof p.node.collapsable && p.node.collapsable !== !0), p.collapsable ? p.collapsed = !(!angular.isUndefined(p.node.collapsed) && "boolean" == typeof p.node.collapsed && p.node.collapsed !== !0) : p.collapsed = !1, p.node.state === i.current.name || i.includes(p.node.state)) {
			if (angular.isDefined(p.node.stateParams) && angular.isDefined(i.params) && !angular.equals(p.node.stateParams, i.params)) return;
			e.$emit("artNavigation::stateMatched"), o.setActiveItem(p.node, e)
		}
		console.log("p test", p, p.node, e.node);
		e.$on("artNavigation::stateMatched", function() {
			p.collapsable && p.collapsed && e.$evalAsync(function() {
				p.collapsed = !1
			})
		}), e.$on("artNavigation::collapse", function(e, t) {
			console.log('$on("artNavigation::collapse"', t, p, p.collapsed, p.collapsable);
			if (!p.collapsed && p.collapsable)
				if (angular.isUndefined(t)) p.collapse();
				else {
					var a = t.split("."),
							n = [],
							i = o.getActiveItem();
					if (i && (n = i.node._path.split(".")), a.indexOf(p.node._id) > -1) return;
					if (n.indexOf(p.node._id) > -1) return;
					p.collapse()
				}
		}), e.$on("$stateChangeSuccess", function() {
			if (p.node.state === i.current.name) {
				if (angular.isDefined(p.node.stateParams) && angular.isDefined(i.params) && !angular.equals(p.node.stateParams, i.params)) return;
				o.setActiveItem(p.node, e), a.$broadcast("artNavigation::collapse", p.node._path)
			}
			if (i.includes(p.node.state)) {
				if (angular.isDefined(p.node.stateParams) && angular.isDefined(i.params) && !angular.equals(p.node.stateParams, i.params)) return;
				e.$emit("artNavigation::stateMatched")
			}
		})
	}

	function l() {
		p.collapsed ? p.expand() : p.collapse()
	}

	function d() {
		console.log('collapse', p, p.element, p.element.children("ul"));
		var t = p.element.children("ul"),
				a = t[0].offsetHeight;
		e.$evalAsync(function() {
			p.collapsed = !0, p.element.addClass("collapsing"), n.animate(t, {
				display: "block",
				height: a + "px"
			}, {
				height: "0px"
			}, p.animateHeightClass).then(function() {
				t.css({
					display: "",
					height: ""
				}), p.element.removeClass("collapsing")
			}), e.$broadcast("artNavigation::collapse")
		})
	}

	function r() {
		var t = p.element.children("ul");
		t.css({
			position: "absolute",
			visibility: "hidden",
			display: "block",
			height: "auto"
		});
		var i = t[0].offsetHeight;
		t.css({
			position: "",
			visibility: "",
			display: "",
			height: ""
		}), e.$evalAsync(function() {
			p.collapsed = !1, p.element.addClass("expanding"), n.animate(t, {
				display: "block",
				height: "0px"
			}, {
				height: i + "px"
			}, p.animateHeightClass).then(function() {
				t.css({
					height: ""
				}), p.element.removeClass("expanding")
			}), a.$broadcast("artNavigation::collapse", p.node._path)
		})
	}

	function c() {
		return p.node["class"]
	}

	function m() {
		return angular.isDefined(p.node.hidden) && angular.isFunction(p.node.hidden) ? p.node.hidden() : !1
	}
	var p = this;
	p.element = t, p.node = e.node, p.hasChildren = void 0, p.collapsed = void 0, p.collapsable = void 0, p.group = void 0, p.animateHeightClass = "animate-height", p.toggleCollapsed = l, p.collapse = d, p.expand = r, p.getClass = c, p.isHidden = m, s()
}