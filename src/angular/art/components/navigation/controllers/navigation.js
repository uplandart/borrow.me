angular.module("art.navigation").controller("ArtNavigationController", ArtNavigationController);

ArtNavigationController.$inject = ["$scope", "artNavigationService"];
function ArtNavigationController($scope, artNavigationService) {

	let vm = this;

	vm.navigation = artNavigationService.getNavigation($scope.root);
	vm.toggleHorizontalMobileMenu = () => {
		angular.element("body").toggleClass("art-navigation-horizontal-mobile-menu-active");
	};

	artNavigationService.sort();
}