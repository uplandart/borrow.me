angular.module("art.navigation")
		.provider("artNavigationService", artNavigationService);

function artNavigationService() {

	let $log = angular.injector(["ng"]).get("$log"),
			l = [],
			self = this;

	self.saveItem = saveItem;
	self.deleteItem = deleteItem;
	self.sortByWeight = sort;

	this.$get = function() {

		let v = null,
				b = null,
				f = null,
				y = null;

		return {
			saveItem,
			deleteItem: deleteItem,
			sort,
			clearNavigation,
			setActiveItem,
			getActiveItem,
			getNavigation,
			getFlatNavigation,
			setNavigationScope,
			setFolded,
			getFolded,
			setFoldedOpen,
			getFoldedOpen,
			toggleFolded
		};

		function clearNavigation() {
			l = [];
			if (b) b.vm.navigation = l;
		}

		function setActiveItem(e, t) {
			v = {
				node: e,
				scope: t
			};
		}

		function getActiveItem() {
			return v;
		}

		function getNavigation(e) {
			if (e) {
				for (let t = 0; t < l.length; t++) {
					if (l[t]._id === e) return [l[t]];
				}
				return null;
			}
			return l;
		}

		function getFlatNavigation(e) {
			let t = getNavigation(e);
			return g(t);
		}

		function setNavigationScope(e) {
			b = e;
		}

		function setFolded(e) {
			f = e;
		}

		function getFolded() {
			return f;
		}

		function setFoldedOpen(e) {
			y = e;
		}

		function getFoldedOpen() {
			return y;
		}

		function toggleFolded() {
			b.toggleFolded();
		}

		function g(e) {
			let res = [];
			for (let i = 0; i < e.length; i++) {
				let n = angular.copy(e[i]);
				n.children = [];
				res.push(n);
				if (e[i].children.length > 0) {
					res = res.concat(g(e[i].children));
				}
			}
			return res;
		}
	};

	function saveItem(e, t) {
		if (!angular.isString(e)) {
			$log.error("path must be a string (eg. `dashboard.project`)");
			return;
		}

		let a = e.split("."),
				i = a[a.length - 1],
				l = n(a),
				d = false;

		for (let r = 0; r < l.length; r++) {
			if (l[r]._id === i) {
				d = l[r];
				break;
			}
		}

		// d ? (angular.extend(d, t), d.uisref = o(d)) : (t.children = [], (angular.isUndefined(t.weight) || !angular.isNumber(t.weight)) && (t.weight = 1), t._id = i, t._path = e, t.uisref = o(t), l.push(t));
		if (d) {
			angular.extend(d, t);
			d.uisref = o(d);
		} else {
			t.children = [];
			if (angular.isUndefined(t.weight) || !angular.isNumber(t.weight)) {
				t.weight = 1;
			}
			t._id = i;
			t._path = e;
			t.uisref = o(t);
			l.push(t);
		}
	}

	function deleteItem(e) {
		if (!angular.isString(e)) {
			$log.error("path must be a string (eg. `dashboard.project`)");
			return;
		}

		let t = l,
				a = e.split(".");

		for (let n = 0; n < a.length; n++) {
			let i = a[n];

			for (let o = 0; o < t.length; o++)
				if (t[o]._id === i) {
					if (t[o]._path === e) {
						t.splice(o, 1);
						return true;
					}
					t = t[o].children;
					break;
				}
		}
		return false;
	}

	function sort(e) {
		if (!e) {
			e = l;
			e.sort(i);
		}
		for (let t = 0; t < e.length; t++) {
			let children = e[t].children;
			if (children.length > 1) children.sort(i);
			if (children.length > 0) sort(children);
		}
	}

	function n(e) {
		let t = l;
		if (1 === e.length) return t;
		e.pop();

		for (let a = 0; a < e.length; a++) {
			let n = e[a],
					i = true;

			for (let o = 0; o < t.length; o++) {
				if (t[o]._id === n) {
					t = t[o].children;
					i = false;
					break;
				}
			}

			if (i) {
				let s = {
					_id: n,
					_path: e.join("."),
					title: n,
					weight: 1,
					children: []
				};
				t.push(s);
				t = s.children;
			}
		}
		return t;
	}

	function i(e, t) {
		return parseInt(e.weight) - parseInt(t.weight);
	}

	function o(e) {
		let res = "";
		// return angular.isDefined(e.state) && (res = e.state, angular.isDefined(e.stateParams) && angular.isObject(e.stateParams) && (res = res + "(" + angular.toJson(e.stateParams) + ")")), res;
		if (angular.isDefined(e.state)) {
			res = e.state;
			if (angular.isDefined(e.stateParams) && angular.isObject(e.stateParams)) {
				res = res + "(" + angular.toJson(e.stateParams) + ")";
			}
		}
		return res;
	}
}