angular.module("art.splashScreen").directive("artSplashScreen", artSplashScreen);

artSplashScreen.$inject = ["$animate", "$timeout"];
function artSplashScreen($animate, $timeout) {
	return {
		restrict: "E",
		link: function(scope, element) {
			var n = scope.$on("artSplashScreen::remove", function() {
				// $timeout(function () {
					$animate.leave(element).then(function() {
						n();
						scope = element = null;
					});
				// }, 3000);

			})
		}
	}
}