angular.module("app")
.config(ConfigRoutes);

ConfigRoutes.$inject = ['$stateProvider', '$urlRouterProvider', 'artNavigationServiceProvider'];
function ConfigRoutes($stateProvider, $urlRouterProvider, artNavigationServiceProvider) {

	$urlRouterProvider.otherwise('/home');
	$stateProvider

			.state("app", {
				"abstract": true,
				views: {
					"main@": {
						templateUrl: "/angular/core/directives/layout/vertical-navigation/vertical-navigation.html",
						controller: "MainCtrl as vm"
					},
					"toolbar@app": {
						templateUrl: "/angular/core/directives/toolbar/vertical-navigation/toolbar.html",
						controller: "ToolbarCtrl as vm"
					},
					"navigation@app": {
						templateUrl: "/angular/core/directives/navigation/vertical-navigation/navigation.html",
						controller: "NavigationCtrl as vm"
					}
				}
			})

			.state("app.home", {
				url:"/home",
				views: {
					"content@app": {
						templateUrl: "/cabinet/assets/angular/pages/home/directives/home.html",
						controller: "HomeCtrl as vm"
					}
				},
				bodyClass: "home"
			});

	artNavigationServiceProvider.saveItem("apps", {
		title: "APPS",
		group: !0,
		weight: 1
	});
	artNavigationServiceProvider.saveItem("apps.to-do", {
		title: "To-Do",
		icon: "checkbox-marked",
		state: "app.to-do",
		badge: {
			content: 3,
			color: "#FF6F00"
		},
		weight: 9
	});
}