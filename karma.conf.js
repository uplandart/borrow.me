// Karma configuration
// Generated on Sun Jul 09 2017 11:04:06 GMT+0300 (MSK)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      './public/assets/js/libs/jquery.min.js',
      './public/assets/js/libs/jquery.dataTables.min.js',
      './public/assets/js/libs/mobile-detect.min.js',
      './public/assets/js/libs/perfect-scrollbar.min.js',
      './public/assets/js/libs/moment.min.js',

      './public/assets/js/libs/angular.core.js',

      './public/assets/angular/art.bundle.js',
      './public/assets/angular/app.core.bundle.js',

      './src/bower/angular-mocks/angular-mocks.js',

      './src/angular/art/**/*.spec.js',
      './src/angular/core/**/*.spec.js',

      './src/angular/login/**/*.spec.js',
      './src/angular/cabinet/**/*.spec.js',
      './src/angular/admin/**/*.spec.js',
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome','Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
};
