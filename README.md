# borrow.me

Storage information about your borrowing

## Environment

### Back End
* [node.js](https://nodejs.org/en/) (v8.0.0)
* [postgresql](https://www.postgresql.org/) (v9.4.5)

### Front End
* [angular.js](https://angularjs.org/) (v1.6.5)
* [angular-material](https://material.angularjs.org/1.1.4/) (v1.1.4)
* [pug](https://pugjs.org)
* [stylus](http://stylus-lang.com/)

## First Init

```
npm install
```
will install all modules (node_modules, bower_modules)

## Client Build

* build (production)
```
npm run build
```
* build and start listening changes (production)
```
npm run builder
```
* build (development)
```
npm run build-dev
```
* build and start listening changes (development)
```
npm run builder-dev
```

## Server Start

* production
```
@KEY=VALUE pm2 start pm2.config.js
```
* development
```
@KEY=VALUE pm2-dev start pm2.config.js
```

`@KEY=VALUE` - this is the node.js environment variables,  
that need to define for your configuration